require 'net/http'
require 'benchmark'

class StatusFetcher
  attr_reader :result

  def initialize
    @result = []
  end

  def self.start!
    new
  end
end

class ThreadStatusFetcher < StatusFetcher
  def initialize
    super
    threads = []
    %w{ stackoverflow.com
        superuser.com
        systemfault.com
      }.each do |page|
      threads << Thread.new(page) do |url|
        http = Net::HTTP.new(url, 80)
        sleep 1
        @result << "#{url}: #{http.get('/').message}"
      end
    end
    threads.each(&:join)
  end
end

class SimpleStatusFetcher < StatusFetcher
  def initialize
    super
    %w{ stackoverflow.com
        superuser.com
        systemfault.com
      }.each do |url|
      http = Net::HTTP.new(url, 80)
      sleep 1
      @result << "#{url}: #{http.get('/').message}"
    end
  end
end

def simplestartup
  result = []
  20.times do
    result << SimpleStatusFetcher.start!.result
  end
end

def threadstartup
  result = []
  20.times do
    result << ThreadStatusFetcher.start!.result
  end
end

Benchmark.bm(8) do |r|
  #r.report('simple') { simplestartup }
  r.report('thread') { threadstartup }
end